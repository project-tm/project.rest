
CREATE TABLE IF NOT EXISTS `be_block` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PARENT` int(10) unsigned NOT NULL DEFAULT '0',
  `IS_ROOT` enum('1','0') NOT NULL,
  `ELEMENT_TYPE` enum('iblock','master','template') NOT NULL DEFAULT 'iblock',
  `ELEMENT_ID` int(10) unsigned NOT NULL DEFAULT '0',
  `PROPS_ID` int(10) unsigned NOT NULL DEFAULT '0',
  `SORT` int(10) unsigned NOT NULL DEFAULT '0',
  `OPEN` enum('1','0') NOT NULL DEFAULT '0',
  `TYPE` enum('block.accordion','block.files','block.navigate','block.news','block.note','block.tab','block.tabs','block.text','block.userfiles','block.xls','block.master','block.template') NOT NULL,
  `MASTER_ID` int(10) unsigned NOT NULL DEFAULT '0',
  `HEADER` text NOT NULL,
  `HEADER_SIZE` int(2) unsigned NOT NULL,
  `HEADER_COLOR` varchar(8) NOT NULL,
  `HEADER_IS_SHOW` enum('1','0') NOT NULL,
  `HEADER_IS_NAV` enum('1','0') NOT NULL,
  `PADDING_BOTTOM` enum('1','0') NOT NULL,
  `BORDER` enum('1','0') NOT NULL,
  `ICON` int(10) unsigned NOT NULL DEFAULT '0',
  `PARAM` text,
  `CONTENT` longtext NOT NULL,
  `REGION` longtext,
  `FILIAL` longtext,
  `ROLE` longtext,
  `CLIENT_TYPE` longtext,
  PRIMARY KEY (`ID`),
  KEY `ELEMENT_ID` (`ELEMENT_TYPE`, `ELEMENT_ID`),
  KEY `ELEMENT_ID_SORT` (`ELEMENT_TYPE`, `ELEMENT_ID`,`SORT`),
  KEY `ELEMENT_ID_PROPS_ID` (`ELEMENT_TYPE`, `ELEMENT_ID`,`PROPS_ID`),
  KEY `TYPE` (`ELEMENT_TYPE`, `MASTER_ID`),
  KEY `ELEMENT_ID_PROPS_ID_SORT` (`ELEMENT_TYPE`, `ELEMENT_ID`,`PROPS_ID`,`SORT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `be_block_client_type` (
  `BLOCK_ID` int(10) unsigned NOT NULL,
  `CLIENT_TYPE` int(10) unsigned NOT NULL,
  PRIMARY KEY (`BLOCK_ID`,`CLIENT_TYPE`),
  KEY `be_block_client_type_BLOCK_ID_foreign` (`BLOCK_ID`),
  CONSTRAINT `be_block_client_type_ibfk_1` FOREIGN KEY (`BLOCK_ID`) REFERENCES `be_block` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `be_block_filial` (
  `BLOCK_ID` int(10) unsigned NOT NULL,
  `FILIAL` int(10) unsigned NOT NULL,
  PRIMARY KEY (`BLOCK_ID`,`FILIAL`),
  KEY `be_block_filial_BLOCK_ID_foreign` (`BLOCK_ID`),
  CONSTRAINT `be_block_filial_ibfk_1` FOREIGN KEY (`BLOCK_ID`) REFERENCES `be_block` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `be_block_region` (
  `BLOCK_ID` int(10) unsigned NOT NULL,
  `REGION` int(10) unsigned NOT NULL,
  PRIMARY KEY (`BLOCK_ID`,`REGION`),
  KEY `be_block_region_BLOCK_ID_foreign` (`BLOCK_ID`),
  CONSTRAINT `be_block_region_ibfk_1` FOREIGN KEY (`BLOCK_ID`) REFERENCES `be_block` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `be_block_role` (
  `BLOCK_ID` int(10) unsigned NOT NULL,
  `ROLE` int(10) unsigned NOT NULL,
  PRIMARY KEY (`BLOCK_ID`,`ROLE`),
  KEY `be_block_role_BLOCK_ID_foreign` (`BLOCK_ID`),
  CONSTRAINT `be_block_role_ibfk_1` FOREIGN KEY (`BLOCK_ID`) REFERENCES `be_block` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `be_block_xls` (
  `BLOCK_ID` int(10) unsigned NOT NULL,
  `XLS_ID` int(10) unsigned NOT NULL,
  `FILE_NAME` text COLLATE utf8_unicode_ci NOT NULL,
  `ROWS` int(10) unsigned NOT NULL,
  `UNUQUIE` text COLLATE utf8_unicode_ci NOT NULL,
  `HEADER` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`XLS_ID`),
  KEY `BLOCK_ID` (`BLOCK_ID`),
  KEY `XLS_ID` (`XLS_ID`),
  CONSTRAINT `be_block_xls_data_ibfk_1` FOREIGN KEY (`BLOCK_ID`) REFERENCES `be_block` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `be_block_xls_data` (
  `XLS_ID` int(10) unsigned NOT NULL DEFAULT '0',
  `ROWS_ID` int(10) unsigned NOT NULL DEFAULT '0',
  `COLD_ID` int(10) unsigned NOT NULL DEFAULT '0',
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`XLS_ID`,`ROWS_ID`,`COLD_ID`),
  KEY `XLS_ID` (`XLS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
