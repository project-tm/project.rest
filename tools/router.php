<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Project\Editor\Router;

define('STOP_STATISTICS', true);
define("NO_KEEP_STATISTIC", true);
define('NO_AGENT_STATISTIC', true);
define("NOT_CHECK_PERMISSIONS", true);
define('BX_NO_ACCELERATOR_RESET', true);
define("STOP_WEBDAV", true);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$arResult = [];
try {
    if (!$USER->IsAuthorized()) {
        CHTTP::SetStatus("401 Unauthorized");
        throw new Exception('Необходимо авторизоваться', 401);
    }
    if (!Loader::includeModule('project.editor')) {
        CHTTP::SetStatus("500 Internal Server Error");
        throw new Exception('Модуль project.editor не подключен', 500);
    }

    $path = Application::getInstance()->getContext()->getRequest()->get('ROUTER');
    if (empty($path)) {
        CHTTP::SetStatus("406 Not Acceptable");
        throw new Exception('Не указан запрос', 400);
    }
    Router::dispatch($path);

} catch (Exception $e) {
    header('Content-Type: application/json');
    if (CHTTP::GetLastStatus() === '200 OK') {
        CHTTP::SetStatus('400 Bad Request');
    }
    echo json_encode([
        'code'    => $e->getCode(),
        'message' => $e->getMessage(),
    ]);
}
