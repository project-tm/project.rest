<?php

namespace Project\Editor;

use CHTTP;

class Response
{

    const ERROR = [
        400 => '400 Bad Request',
        401 => '401 Unauthorized',
        404 => '404 Not Found',
    ];

    /**
     * @param $message
     */
    public static function erorr400($message)
    {
        self::erorr($message, 400);
    }

    /**
     * @param $message
     */
    public static function erorr404($message)
    {
        self::erorr($message, 404);
    }

    /**
     * @param $message
     * @param $code
     */
    public static function erorr($message, $code)
    {
        self::send([
            'code'    => $code,
            'message' => $message,
        ], empty(self::ERROR[$code]) ? $code : self::ERROR[$code]);
    }


    /**
     * @param        $arResult
     * @param string $code
     */
    public static function send($arResult, $code = '200 OK')
    {
        header('Content-Type: application/json');
        if (CHTTP::GetLastStatus() === $code) {
            CHTTP::SetStatus($code);
        }
        echo json_encode($arResult);
        exit;
    }

    public static function success()
    {
        self::send([
            'message' => 'Успех',
        ]);
    }

}
