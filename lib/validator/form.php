<?php

namespace Project\Editor\Validator;

use Bitrix\Main\Application;
use Exception;

class Form
{
    /**
     * @param $name
     *
     * @return null|string
     * @throws \Bitrix\Main\SystemException
     */
    public static function getPost($name)
    {
        return Application::getInstance()->getContext()->getRequest()->getPost($name);
    }

    /**
     * @param      $name
     * @param null $default
     *
     * @return null
     */
    public static function getJson($name, $default = null)
    {
        $arData = self::getJsonAll();
        return isset($arData[$name]) ? $arData[$name] : $default;
    }

    /**
     * @return mixed|null
     */
    public static function getJsonAll()
    {
        static $arData = null;
        if (is_null($arData)) {
            try {
                $arData = json_decode(file_get_contents('php://input'), true);
            } catch (Exception $e) {
                $arData = [];
            }
        }
        return $arData;
    }

    /**
     * @param $arData
     * @param $arParam
     *
     * @return array
     * @throws Exception
     */
    public static function parseForm($arParam, $arData = false)
    {
        if (empty($arData)) {
            $arData = self::getJsonAll();
        }
        $arResult = $arError = [];
        foreach ($arParam as $name => $config) {
            if (is_array($config)) {
                $type = isset($config['type']) ? $config['type'] : 'string';
            } else {
                $type = $config;
                $config = [];
            }
            $arName = explode('-', $name);
            switch (count($arName)) {
                case 1:
                    $value = isset($arData[$arName[0]]) ? $arData[$arName[0]] : null;
                    $arResult[$arName[0]] = self::parseItem($type, $arName[0], $config, $value, $arError);
                    break;
                case 2:
                    $value = isset($arData[$arName[0]][$arName[1]]) ? $arData[$arName[0]][$arName[1]] : null;
                    $arResult[$arName[0]][$arName[1]] = self::parseItem($type, $arName[1], $config, $value, $arError);
                    break;
            }
        }
        if ($arError) {
            throw new Exception(implode('<br>', $arError), 400);
        }
        return $arResult;
    }

    public static function parseItem($type, $name, $config, $value, &$arError)
    {
        switch ($type) {
            case 'bool':
                return (string)(int)$value;
                break;
            case 'array-int':
                if (!empty($value) && !is_array($value)) {
                    $arError[] = sprintf('Поле %s должен быть массив', $name);
                }
                return $value ? array_map('ceil', $value) : [];
                break;
            case 'array-bool':
                if (!empty($value) && !is_array($value)) {
                    $arError[] = sprintf('Поле %s должен быть массив', $name);
                }
                foreach ($value as $k => $v) {
                    $value[$k] = (bool)$v;
                }
                return $value;
                break;
            case 'array-string':
                if (!empty($value) && !is_array($value)) {
                    $arError[] = sprintf('Поле %s должен быть массив', $name);
                }
                foreach ($value as $k => $v) {
                    $value[$k] = (string)$v;
                }
                return $value;
                break;
            case 'int':
                $value = (int)$value;
                if (isset($config['required']) and empty($value)) {
                    $arError[] = sprintf('Поле %s должно быть заполнено', $name);
                }
                return $value;
                break;
            case 'string':
                $value = (string)$value;
                if (isset($config['required']) and empty($arData[$name])) {
                    $arError[] = sprintf('Поле %s не заполнено', $name);
                } else {
                    if (isset($config['min']) and strlen($value) < $config['min']) {
                        $arError[] = sprintf('Поле %s короче %s символов', $name, $config['min']);
                    }
                    if (isset($config['max']) and strlen($value) > $config['max']) {
                        $arError[] = sprintf('Поле %s длинее %s символов', $name, $config['min']);
                    }
                }
                return $value;
                break;
            default:
                return $value;
                break;
        }
    }
}