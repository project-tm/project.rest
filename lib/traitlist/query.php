<?php

namespace Project\Editor\TraitList;

use Project\Editor\Entity;

trait Query {

    public static function query()
    {
        return new Entity\Query(static::getEntity());
    }

}