<?php

namespace Project\Editor;

use Project\Editor\Property\BlockType;

class Config
{

    /* Свойство Блочный редактор */
    const USER_TYPE_BLOCK_EDITOR = BlockType::class;
    /* Типы блоков */

    const BLOCK_TYPE_IBLOCK = 'iblock';
    const BLOCK_TYPE_MASTER = 'master';
    const BLOCK_TYPE_TEMPLATE = 'template';
    const BLOCK_TYPE_PROPERTY = 'BLOCK_EDITOR';

    const DB_BLOCK_MASTER = 'block.master';
    const DB_BLOCK_TEMPLATE = 'block.template';

    const IBLOCK_TYPE = 'iblock_editor2';
    const IBLOCK_MASTER = 'iblock_editor2_master';
    const IBLOCK_LOG = 'iblock_editor2_log';
    const IBLOCK_TEMPLATE = 'iblock_editor2_template';

    const IBLOCK_ELEMENT_URL = '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=#IBLOCK_ID#&type=#IBLOCK_TYPE_ID#&IBLOCK_SECTION_ID=#IBLOCK_SECTION_ID#&ID=#ID#&lang=ru&find_section_section=-1&WF=Y';

    /* Медиабиблиотека */
    const МEDIALIB_ICON = 'Иконки блоков БР';
    const МEDIALIB_ICON_CODE = 'MEGAFON_EDITOR_ICON';

    /* запись данных */
    const BIGDATA_LIMIT = 100;

    /* xls */
    const XLS_UNUQUIE_LIMIT = 100;

    /* Диск */
    const STORAGE_ID = 4891;
    const STORAGE_XLS = '[Не трогать] Блочный редактор - xlsx файлы';
    const STORAGE_XLS_LIMIT = 4;
    const STORAGE_TYPE_FILE = 3;
    const STORAGE_TYPE_FOLDER = 2;

}