<?php

namespace Project\Editor\Controller;

use Bitrix\Main\Application;
use Project\Editor\Model;
use Project\Editor\Helper;
use Project\Editor\Validator\Form;

class Disk
{

    const RULES_NAME = [
        "NAME"      => [
            'type' => 'string',
            'min'  => 1,
            'max'  => 250,
        ],
    ];

    /**
     * @param int $FOLDER_ID
     *
     * @return array
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function getList($FOLDER_ID = 0)
    {
        return Model\Disk::getList($FOLDER_ID, [
            'ID',
            'NAME',
            'TYPE',
            'SIZE',
            'TIME' => 'UPDATE_TIME',
        ]);
    }

    /**
     * @param $fileId
     * @param $type
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function delete($fileId, $type)
    {
        $FOLDER_ID = Model\Disk::delete($fileId, $type);
        return self::getList($FOLDER_ID);
    }

    /**
     *
     * @param int $FOLDER_ID
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    static public function create($FOLDER_ID = 0)
    {
        $data = Form::parseForm(self::RULES_NAME);
        Model\Disk::create($FOLDER_ID, $data['NAME']);
        return self::getList($FOLDER_ID);
    }

    /**
     * @param $fileId
     * @param $type
     *
     * @return array
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    static public function rename($fileId, $type)
    {
        $data = Form::parseForm(self::RULES_NAME);
        $FOLDER_ID = Model\Disk::rename($fileId, $type, $data['NAME']);
        return self::getList($FOLDER_ID);
    }


    /**
     * @param int $FOLDER_ID
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Exception
     */
    static public function upload($FOLDER_ID = 0)
    {
        $arFile = Helper\Disk::getUploadFile('FILE');
        Model\Disk::upload($FOLDER_ID, $arFile);
        return self::getList($FOLDER_ID);
    }

    /**
     * @param $fileId
     *
     * @return array
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Exception
     */
    static public function updateUpload($fileId)
    {
        $arFile = Helper\Disk::getUploadFile('FILE');
        $FOLDER_ID = Model\Disk::updateUpload($fileId, $arFile);
        return self::getList($FOLDER_ID);
    }

}