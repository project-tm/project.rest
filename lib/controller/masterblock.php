<?php

namespace Project\Editor\Controller;

use Project\Editor\Model;
use Project\Editor\Response;
use Project\Editor\Validator\Form;
use Exception;

class MasterBlock
{

    const RULES = [
        "RUBRIC_ID" => [
            'type'     => 'int',
        ],
        "ACTIVE"    => 'bool',
        "SORT"      => 'int',
        "NAME"      => [
            'type' => 'string',
            'min'  => 5,
            'max'  => 250,
        ],
    ];

    /**
     * Создает 'Мастер-блок БР'
     *
     * @return array
     * @throws Exception
     */
    static public function create()
    {
        $data = Form::parseForm(self::RULES);
        $ID = Model\MasterBlock::add($data, Form::getJson('BLOCK'));
        if ($ID) {
            return [
                'ID'           => $ID,
                'templateList' => Model\MasterBlock::getByRubric($data['RUBRIC_ID']),
            ];
        } else {
            throw new Exception('Не правильные данные', 400);
        }
    }

    /**
     * Возвращает 'Мастер-блок БР'
     *
     * @param $id
     *
     * @return array $result
     * @throws Exception
     */
    static public function getById($id)
    {
        $result = Model\MasterBlock::getById($id);
        if ($result) {
            return $result;
        } else {
            throw new Exception('Не найден', 404);
        }
    }

    /**
     * Возвращает массив 'Мастер-блоков БР' рубрики
     *
     * @param $id
     *
     * @return array $result
     * @throws Exception
     */
    static public function getByRubric($id)
    {
        $result = Model\MasterBlock::getByRubric($id);
        if ($result) {
            return $result;
        } else {
            throw new Exception('Мастер-блоки не найдены', 404);
        }
    }

    /**
     * Возращает список кинфоблоков с элементами, где используются
     *
     * @param $id
     *
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function useById($id)
    {
        $result = Model\MasterBlock::useById($id);
        if ($result !== false) {
            return $result;
        } else {
            throw new Exception('Не найден', 404);
        }
    }

    /**
     * Обновляет 'Мастер-блок БР'
     *
     * @param $id
     *
     * @return array
     * @throws Exception
     */
    static public function update($id)
    {
        $data = Form::parseForm(self::RULES);
        Model\MasterBlock::update($id, $data, Form::getJson('BLOCK'));
        return Model\MasterBlock::getByRubric($data['RUBRIC_ID']);
    }

    /**
     * Удаляет 'Мастер-блок БР'
     *
     * @param $id
     *
     * @throws Exception
     */
    static public function delete($id)
    {
        Model\MasterBlock::delete($id);
        Response::success();
    }
}