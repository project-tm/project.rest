<?php

namespace Project\Editor\Controller\Block;

use Adv\Core\Utils;
use Bitrix\Main\Application;
use Bitrix\Main\Entity\ExpressionField;
use Exception;
use Project\Editor\Database;
use Project\Editor\Helper;
use Project\Editor\Model;
use Project\Editor\Validator\Form;

class Xls
{

    /**
     * @param $BLOCK_ID
     *
     * @return array
     * @throws Exception
     */
    static public function getById($BLOCK_ID)
    {
        $arXls = Model\Block\Xls::getBlock($BLOCK_ID);
        return $arXls['TABLE'];
    }

    /**
     * @param $BLOCK_ID
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    static public function searchById($BLOCK_ID)
    {
        $arXls = Model\Block\Xls::getBlock($BLOCK_ID);
        $obXls = $arXls['CONFIG'];

        $arParam = [
            'select' => [new ExpressionField('ROWS_ID_UNIQUIRE', 'SQL_CALC_FOUND_ROWS DISTINCT %s', 'ROWS_ID')],
            'limit'  => $obXls->PAGE_LIMIT ?: 20,
            'filter' => [
                'XLS_ID' => $obXls->FILE_ID,
            ],
        ];
        $arResult = [];
        if (Helper\Router::getMethod() === 'POST') {
            $query = trim(Form::getJson('SEARCH'));
            if ($query) {
                $arParam['filter']['VALUE'] = $query;
            }

            $filterArr = Form::getJson('FILTER', []);
            if (is_array($filterArr) && count($filterArr)) {
                $filter = [];
                foreach ($filterArr as $filterItem) {
                    $filterItem['QUERY'] = trim($filterItem['QUERY']);
                    if (strlen($filterItem['QUERY'])) {
                        if (!$filter[$filterItem['COLS']]) {
                            $filter[$filterItem['COLS']]['LOGIC'] = 'OR';
                        }
                        $filter[$filterItem['COLS']][] = [
                            'VALUE'   => $filterItem['QUERY'],
                            'COLD_ID' => $filterItem['COLS'],
                        ];
                    }
                }

                if (count($filter)) {
                    $arParam['filter'][] = array_merge(['LOGIC' => 'OR'], $filter);
                }
            }

            $page = (int)Form::getJson('PAGE');
            if ($page > 1) {
                $arParam['offset'] = ($page - 1) * $obXls->PAGE_LIMIT;
            }

            $order = Form::getJson('ORDER', []);
            if (
                isset($order['COLS'])
                && isset($order['TYPE']) && ($order['TYPE'] === 'ASC' || $order['TYPE'] === 'DESC')
            ) {
                $arParam['order']['VALUE'] = $order['TYPE'];
                $arParam['filter']['COLD_ID'] = $order['COLS'];
            }

            $res = Database\Block\XlsDataTable::getList([
                'select' => $arParam['select'],
                'filter' => $arParam['filter'],
            ]);
            $arParam['select'] = [new ExpressionField('ROWS_ID_UNIQUIRE', 'DISTINCT %s', 'ROWS_ID')];
            $arParam['filter']['ROWS_ID'] = [];
            $arResult['CNT'] = $res->getSelectedRowsCount();

            while ($arItem = $res->fetch()) {
                $arParam['filter']['ROWS_ID'][] = $arItem['ROWS_ID_UNIQUIRE'];
            }
            if (empty($arParam['filter']['ROWS_ID'])) {
                return [];
            }
            unset($arParam['filter']['VALUE']);
        }
        if ($obXls->IS_SEARCH_VIEW) {
            if (empty($query)) {
                throw new Exception('Укажите исковый запрос', 400);
            }
        }

        $res = Database\Block\XlsDataTable::getList($arParam);
        while ($arItem = $res->fetch()) {
            $arResult['DATA'][$arItem['ROWS_ID_UNIQUIRE']] = [];
        }
        if (empty($arResult['CNT'])) {
            $arResult['CNT'] = Application::getConnection()->query("SELECT FOUND_ROWS() AS ROWS")->Fetch()['ROWS'];
        }

        if (empty($arResult['DATA'])) {
            return [];
        }

        $res = Database\Block\XlsDataTable::getList([
            'select' => ['ROWS_ID', 'COLD_ID', 'VALUE'],
            'filter' => [
                'XLS_ID'  => $obXls->FILE_ID,
                'ROWS_ID' => array_keys($arResult['DATA']),
            ],
        ]);

        while ($arItem = $res->fetch()) {
            $arResult['DATA'][$arItem['ROWS_ID']][$arItem['COLD_ID']] = $arItem['VALUE'];
        }
        $arResult['DATA'] = array_values($arResult['DATA']);
        return $arResult;
    }

}