<?php

namespace Project\Editor\Controller\Block;

use Project\Editor\Helper;
use Project\Editor\Model;
use Project\Editor\Validator\Form;

class Navigate
{

    /**
     * @param $blockId
     *
     * @throws \Bitrix\Main\ArgumentException
     */
    static public function getById($blockId)
    {
        $arBlock = Helper\Block::getBlockType($blockId, 'block.navigate');
        $arParam = (array)$arBlock['PARAM'];
        $arParam['PAGE'] = Form::getJson('PAGE', 1);
        $arParam['QUERY'] = Form::getJson('QUERY');
        return Model\Block\Navigate::getData($arParam);
    }

    /**
     *
     */
    static public function getData()
    {
        return Model\Block\Navigate::getData(Form::getJsonAll([
            'PAGE'                => 'int',
            'QUERY'               => 'string',
            'ITEMS_COUNT'         => 'int',
            'PATH'                => 'string',
            'INCLUDE_SUB_FOLDERS' => 'string',
            'DELAYED'             => 'string',
        ]));
    }

}