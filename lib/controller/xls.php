<?php

namespace Project\Editor\Controller;

use Exception;
use Project\Editor\Model;
use Project\Editor\Helper;
use Project\Editor\Validator\Form;

class Xls
{

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function getList()
    {
        Helper\Xls::init();
        return Disk::getList(Helper\Xls::getRootId());
    }

    /**
     * @param int $FOLDER_ID
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\SystemException
     */
    static public function upload($FOLDER_ID = 0)
    {
        Helper\Xls::init();
        if (empty($FOLDER_ID)) {
            $FOLDER_ID = Helper\Xls::getRootId();
        }
        $arFile = Helper\Xls::getUploadFile('FILE');
        Helper\Xls::filterType($arFile['name']);
        $fileId = Model\Disk::upload($FOLDER_ID, $arFile);
        return Model\Xls::getFileById($fileId);
    }

    /**
     * @param $XLS_ID
     *
     * @return array
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function getById($XLS_ID)
    {
        return Model\Xls::getFileById($XLS_ID);
    }

    static public function searchById($XLS_ID)
    {
        $start = 0;
        $stop = 20;
        if (Helper\Router::getMethod() === 'POST') {
            $data = Form::parseForm([
                'SEARCH'         => [
                    'type' => 'string',
                ],
                'IS_SEARCH_VIEW' => [
                    'type' => 'bool',
                ],
                'PAGE'           => [
                    'type' => 'int',
                ],
                'PAGE_LIMIT'     => [
                    'type' => 'int',
                ],
                'FILTER'         => [
                    'type' => 'array-string',
                ],
                'ORDER-COLS'     => [
                    'type' => 'int',
                ],
                'ORDER-TYPE'     => [
                    'type' => 'string',
                ],

            ]);
            $page = $data['PAGE'] < 1 ? 1 : $data['PAGE'];
            $limit = $data['PAGE_LIMIT'] < 1 ? 1 : $data['PAGE_LIMIT'];
            $start = $page - 1;
            $stop = $page * $limit;

            $query = $data['SEARCH'];
            $filter = [];
            foreach ($data['FILTER'] as $k => $v) {
                if (!empty($v)) {
                    $filter[$k] = $v;
                }
            }
            $orderCols = $data['ORDER']['COLS'];
            $orderType = $data['ORDER']['TYPE'];
            if ($orderType === 'DESC' || $orderType === 'ASC') {
                $isSort = true;
                $orderType = $orderType== 'DESC';
            }
            if ($data['IS_SEARCH_VIEW']) {
                if (empty($query) && empty($filter)) {
                    throw new Exception('Укажите исковый запрос', 400);
                }
            }
        }

        Helper\Xls::init();
        list($Reader, $file) = Helper\Xls::getReader($XLS_ID);
        $Sheets = $Reader->Sheets();
        $arResult = [];
        $count = 0;
        foreach ($Sheets as $Index => $Name) {
            $Reader->ChangeSheet($Index);
            foreach ($Reader as $rowsId => $row) {
                $row = array_map('trim', $row);
                $notEmpty = false;
                foreach ($row as $coldId => $value) {
                    if(!empty($value)) {
                        $notEmpty = true;
                    }
                }
                if(empty($notEmpty)) {
                    continue;
                }

                if (!empty($query)) {
                    if (empty(in_array($query, $row))) {
                        continue;
                    }
                }
                if (!empty($filter)) {
                    $isSearch = false;
                    foreach ($filter as $k => $v) {
                        if ($row[$k] === $v) {
                            $isSearch = true;
                            break;
                        }
                    }
                    if (empty($isSearch)) {
                        continue;
                    }
                }
                $count++;
                if (empty($isSort) && ($count < $start or $count >= $stop)) {
                    continue;
                }
                $arResult[] = $row;
            }
            break;
        }
        if(isset($isSort)) {
            usort($arResult, function($a, $b) use($orderCols, $orderType) {
                if($orderType) {
                    return $a[$orderCols] < $b[$orderCols];
                } else {
                    return $a[$orderCols] > $b[$orderCols];
                }
            });
            $arResultSort = array();
            foreach ($arResult as $index=>$item) {
                if ($index < $start or $index >= $stop) {
                    continue;
                }
                $arResultSort[] = $item;
            }
            $arResult = $arResultSort;
        }
        return [
            'CNT'    => $count,
            'XML_ID' => $XLS_ID,
            'NAME'   => $file->getOriginalName(),
            'DATA'   => $arResult,
        ];
    }

}