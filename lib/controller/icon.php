<?php

namespace Project\Editor\Controller;

use CFile;
use Exception;
use Project\Editor\Config;
use Project\Editor\Model;

class Icon
{

    /**
     * Возвращает массив медиабиблиотеки коллекции 'Блочный редактор: иконки'
     *
     * @return array
     * @throws Exception
     */
    static public function getList()
    {
        $arResult = [];
        try {
            foreach (Model\Medialib::getList(Config::МEDIALIB_ICON, Config::МEDIALIB_ICON_CODE) as $arItem) {
                $arResult['ITEM'][] = [
                    'ID'   => $arItem['ID'],
                    'NAME' => $arItem['NAME'],
                    'PATH' => $arItem['PATH'],
                ];
            }
        } catch (Exception $e) {
            $arResult['error'] = [
                'code'    => $e->getCode(),
                'message' => $e->getMessage(),
            ];
        }
        return $arResult['ITEM'];
    }

    /**
     * Возвращает массив медиабиблиотеки коллекции 'Иконки блоков БР'
     *
     * @param $id
     *
     * @return array
     * @throws Exception
     */
    static public function getById($id)
    {
        if ($path = CFile::GetPath($id)) {
            return [
                'path' => $path,
            ];
        } else {
            throw new Exception('Иконка не найдена', 404);
        }
    }

}