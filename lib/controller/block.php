<?php

namespace Project\Editor\Controller;

use Project\Editor\Model\BlockTree;
use Project\Editor\Helper;

class Block
{

    /**
     * Получить дерево блоков
     *
     * @param $code
     *
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function getTree($code)
    {
        list($elementId, $propsId, $type) = Helper\Block::parseCode($code);
        return BlockTree::getJson($elementId, $propsId, $type);
    }

}