<?php

namespace Project\Editor\Database\Block;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;
use Project\Editor\TraitList\Query;
use Project\Editor\TraitList\BigData;

class XlsDataTable extends DataManager
{

    use Query;
    use BigData;

    public static function getTableName()
    {
        return 'be_block_xls_data';
    }

    public static function getMap()
    {
        return [
            'XLS_ID'   => new Entity\IntegerField('XLS_ID', [
                'primary'  => true,
                'required' => true,
            ]),
            'ROWS_ID'  => new Entity\IntegerField('ROWS_ID'),
            'COLD_ID'  => new Entity\IntegerField('COLD_ID'),
            'VALUE'    => new Entity\StringField('VALUE'),
        ];
    }

}