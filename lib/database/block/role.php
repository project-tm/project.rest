<?php

namespace Project\Editor\Database\Block;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;
use Project\Editor\Database\BlockTable;
use Project\Editor\TraitList\Query;
use Project\Editor\TraitList\BigData;

class RoleTable extends DataManager
{

    use Query;
    use BigData;

    public static function getTableName()
    {
        return 'be_block_role';
    }

    public static function getMap()
    {
        return [
            'BLOCK_ID' => new Entity\IntegerField('BLOCK_ID', [
                'primary'  => true,
                'required' => true,
                'title'    => 'ID Блока',
            ]),
            'BLOCK'    => new Entity\ReferenceField(
                'BLOCK',
                BlockTable::class,
                ['=this.BLOCK_ID' => 'ref.ID'],
                ['join_type' => 'INNER']
            ),
            'ROLE'     => new Entity\IntegerField('ROLE', [
                'title' => 'ID роли',
            ]),
        ];
    }

}