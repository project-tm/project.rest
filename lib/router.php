<?php

namespace Project\Editor;

use Bitrix\Main\Loader;
use Exception;
use MegafonSystem;
use SpokEdit;

class Router
{

    const CONTROLLER = 'Project\Editor\Controller\\';

    /**
     * все контроллеры находятся в Project\Editor\Contorller
     */
    const PATH = [
        /*
         * Блоки Xls
         */
        '/block/xls/:num' => [
            'GET'  => 'Block\Xls/getById/$1',
            'POST' => 'Block\Xls/getById/$1',
        ],
        '/block/xls/:num/search' => [
            'GET'  => 'Block\Xls/searchById/$1',
            'POST' => 'Block\Xls/searchById/$1',
        ],
        '/block/navigate' => [
            'POST' => 'Block\Navigate/getData',
        ],
    ];
    const PATH_ADMIN = [
        /*
         * xls
         */
        '/disk/xls'                        => [
            'GET' => 'Xls/getList',
        ],
        '/disk/xls(/:num)?/upload'         => [
            'POST' => 'Xls/upload/$2',
        ],
        '/disk/xls/:num'                   => [
            'GET' => 'Xls/getById/$1',
        ],
        '/disk/xls/:num/search' => [
            'GET'  => 'Xls/searchById/$1',
            'POST' => 'Xls/searchById/$1',
        ],
    ];

    /**
     * Роутер
     *
     * @param $path
     *
     * @return mixed
     * @throws Exception
     */
    public static function dispatch($path)
    {
        foreach ([
                     'auth'      => self::PATH,
                     'authAdmin' => self::PATH_ADMIN,
                 ] as $type => $arPath) {
            try {
                $params = null;
                if (!empty($arPath[$path][$_SERVER['REQUEST_METHOD']])) {
                    $params = $arPath[$path][$_SERVER['REQUEST_METHOD']];
                } else {
                    foreach ($arPath as $route => $uri) {
                        if (is_array($uri)) {
                            if (empty($uri[$_SERVER['REQUEST_METHOD']])) {
                                continue;
                            }
                            $uri = $uri[$_SERVER['REQUEST_METHOD']];
                        }
                        if (strpos($route, ':') !== false) {
                            $route = str_replace(':any', '(.+)', str_replace(':num', '([0-9]+)', $route));
                        }
                        if ($route && preg_match('#^' . $route . '\??$#', $path)) {
                            if (strpos($uri, '$') !== false && strpos($route, '(') !== false) {
                                $uri = preg_replace('#^' . $route . '$#', $uri, $path);
                            }
                            $params = $uri;
                            break;
                        }
                    }
                }
                if ($params) {
                    if ($type === 'authAdmin') {
//                        if (Loader::IncludeModule('megafon.spokedit')) {
//                            if (MegafonSystem\Environment::isProd() and !SpokEdit::isMasterServer()) {
//                                throw new Exception('Это не мастер сервер', 400);
//                            }
//                        }
                        if (!\Adv\Helper\User::isAdminOrFkm()) {
                            throw new Exception('Вы не администратор', 401);
                        }
                    }
                    $arResult = self::executeAction(explode('/', $params));
                    Response::send($arResult);
                }
            } catch (Exception $e) {
                Response::erorr($e->getMessage(), $e->getCode());
            }
        }
        throw new Exception('Не поддерживаемый запрос', 404);
    }

    /**
     * Выполняет задачу в контроллере
     *
     * @param $params
     *
     * @return mixed
     */
    private static function executeAction($params)
    {
        $controller = self::CONTROLLER . array_shift($params);
        $action = array_shift($params);
        return call_user_func_array([$controller, $action], $params);
    }

}
