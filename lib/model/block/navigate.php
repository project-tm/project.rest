<?php

namespace Project\Editor\Model\Block;

class Navigate
{

    /**
     * @param $arParam
     */
    static public function getData($arParam)
    {
        global $PAGEN_1;
        $arParam['PAGE'] = empty($arParam['PAGE']) ? 1 : ceil($arParam['PAGE']);
        if (empty($arParam['PAGE']) || $arParam['PAGE'] < 1) {
            $PAGEN_1 = 1;
        } else {
            $PAGEN_1 = (int)$arParam['PAGE'];
        }
        $_REQUEST['q'] = $arParam['QUERY'];
        global $APPLICATION;
        $APPLICATION->IncludeComponent(
            'project:content.pages.iblock.list',
            'json',
            [
                'CONTENT_FOLDER'      => $arParam['PATH'],
                'INCLUDE_SUB_FOLDERS' => $arParam['INCLUDE_SUB_FOLDERS'],
                'ITEMS_COUNT'         => $arParam['ITEMS_COUNT'],
                'DELAYED'             => $arParam['DELAYED'],
                'NAV_TITLE'           => '',
                'NAV_TEMPLATE'        => 'project.1.0',
                'NO_WRAP'             => 'Y',
            ]
        );
    }

}