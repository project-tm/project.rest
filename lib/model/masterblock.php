<?php

namespace Project\Editor\Model;

use Bitrix\Iblock\SectionElementTable;
use Bitrix\Main\Entity\ExpressionField;
use CIBlockElement;
use Exception;
use Project\Editor\Config;
use Project\Editor\Database;
use Project\Editor\Helper;

class MasterBlock
{

    /**
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static protected function getIblockId()
    {
        return Helper\Iblock::getMasterBlockId();
    }

    /**
     * @param $ID
     *
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static protected function getBlock($ID)
    {
        return BlockTree::getJsonMasterBlock($ID);
    }

    /**
     * @param $arBlock
     * @param $ID
     *
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static protected function blockSave($arBlock, $ID)
    {
        Block::saveMaster($arBlock, $ID);
    }

    /**
     * @param array $arFields
     * @param array $arBlock
     *
     * @return bool
     * @throws Exception
     */
    public static function add(array $arFields, array $arBlock)
    {
        if ($arFields['RUBRIC_ID']) {
            $arFilter = [
                'IBLOCK_ID' => static::getIblockId(),
                'ID'        => $arFields['RUBRIC_ID'],
            ];
            if (Helper\Iblock::isNetSection($arFilter)) {
                throw new Exception('Раздел не найден', 400);
            }
        }
        $arFilter = [
            'IBLOCK_ID'  => static::getIblockId(),
            'SECTION_ID' => $arFields['RUBRIC_ID'],
            '=NAME'      => $arFields['NAME'],
        ];
        if (Helper\Iblock::isMasterBlock($arFilter)) {
            throw new Exception('Блок уже существует', 400);
        }

        $el = new CIBlockElement;
        $ID = $el->add([
            'IBLOCK_ID'         => static::getIblockId(),
            'IBLOCK_SECTION_ID' => (int)$arFields['RUBRIC_ID'],
            'NAME'              => $arFields['NAME'],
            'ACTIVE'            => $arFields['ACTIVE'] ? 'Y' : 'N',
            'SORT'              => (int)$arFields['SORT'],
        ]);
        if ($ID) {
            static::blockSave($arBlock, $ID);
            return $ID;
        } else {
            if (empty($res)) {
                throw new Exception($el->LAST_ERROR, 400);
            }
        }
    }

    /**
     * @param $id
     *
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function useById($id)
    {
        if (empty(self::getById($id))) {
            return false;
        }
        $res = Database\BlockTable::getList([
            'select' => [
                new ExpressionField('ELEMENT', 'DISTINCT %s ', 'ELEMENT_ID'),

            ],
            'filter' => [
                '=TYPE'     => Config::DB_BLOCK_MASTER,
                'MASTER_ID' => $id,
            ],
        ]);
        $arFilter = [];
        while ($arItem = $res->Fetch()) {
            $arFilter['ID'][] = $arItem['ELEMENT'];
        }
        if (empty($arFilter)) {
            return [];
        }

        $arResult = [];
        $arSelect = [
            'ID',
            'CODE',
            'ACTIVE_FROM',
            'IBLOCK_ID',
            'IBLOCK_TYPE_ID',
            'IBLOCK_NAME',
            'IBLOCK_SECTION_ID',
            'NAME',
            'DETAIL_PAGE_URL',
        ];
        $res = CIBlockElement::GetList(['NAME' => 'ASC'], $arFilter, false, false, $arSelect);
        while ($arItem = $res->GetNext()) {
            $arResult[$arItem['IBLOCK_ID']]['IBLOCK_ID'] = $arItem['IBLOCK_ID'];
            $arResult[$arItem['IBLOCK_ID']]['NAME'] = $arItem['IBLOCK_NAME'];
            $arResult[$arItem['IBLOCK_ID']]['ITEM'][] = [
                'NAME'     => $arItem['NAME'],
                'VIEW_URL' => Helper\Iblock::viewUrl($arItem),
                'EDIT_URL' => Helper\Site::admin($arItem),
            ];
        }
        return array_values($arResult);
    }


    /**
     * @param $id
     *
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function getById($id)
    {
        $arSelect = [
            'ID',
            'SORT',
            'ACTIVE',
            'IBLOCK_ID',
            'IBLOCK_TYPE_ID',
            'IBLOCK_NAME',
            'IBLOCK_SECTION_ID',
            'NAME',
        ];
        $arFilter = ["IBLOCK_ID" => static::getIblockId(), 'ID' => $id];
        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        if ($arItem = $res->Fetch()) {
            return [
                'RUBRIC_ID' => (int)$arItem['IBLOCK_SECTION_ID'],
                'ACTIVE'    => $arItem['ACTIVE'] == 'Y',
                'SORT'      => (int)$arItem['SORT'] ?: 500,
                'NAME'      => $arItem['NAME'],
                'BLOCK'     => static::getBlock($id),
                'EDIT_URL'  => Helper\Site::admin($arItem),
            ];
        }
        return false;
    }

    /**
     * @param       $id
     * @param array $arFields
     * @param array $arBlock
     *
     * @return mixed
     * @throws Exception
     */
    public static function update($id, array $arFields, array $arBlock)
    {
        if ($arFields['RUBRIC_ID']) {
            $arFilter = [
                'IBLOCK_ID' => self::getIblockId(),
                'ID'        => $arFields['RUBRIC_ID'],
            ];
            if (Helper\Iblock::isNetSection($arFilter)) {
                throw new Exception('Раздел не найден', 400);
            }
        }
        $arFilter = [
            'IBLOCK_ID'  => static::getIblockId(),
            '!ID'        => $id,
            'SECTION_ID' => $arFields['RUBRIC_ID'],
            '=NAME'      => $arFields['NAME'],
        ];
        if (Helper\Iblock::isMasterBlock($arFilter)) {
            throw new Exception('Блок уже существует', 400);
        }
        $el = new CIBlockElement;
        $el->update($id, [
            'IBLOCK_SECTION_ID' => (int)$arFields['RUBRIC_ID'],
            'NAME'              => $arFields['NAME'],
            'ACTIVE'            => $arFields['ACTIVE'] ? 'Y' : 'N',
            'SORT'              => (int)$arFields['SORT'] ?: 500,
        ]);
        if ($el->LAST_ERROR) {
            throw new Exception($el->LAST_ERROR, 400);
        }
        self::blockSave($arBlock, $id);
    }

    /**
     * @param $id
     *
     * @throws Exception
     */
    public static function delete($id)
    {
        $arSelect = ['ID'];
        $arFilter = ["IBLOCK_ID" => static::getIblockId(), 'ID' => $id];
        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        if ($arItem = $res->Fetch()) {
            CIBlockElement::Delete($id);
        } else {
            throw new Exception('Не найден', 404);
        }
    }

    /**
     * @param      $id
     *
     * @param bool $isSection
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getByRubric($id, $isSection = false)
    {
        $arSelect = [
            'ID',
            'NAME',
        ];
        $arFilter = [
            'IBLOCK_ID' => static::getIblockId(),
        ];
        if (empty($isSection)) {
            $arFilter['SECTION_ID'] = $id;
        }
        $res = CIBlockElement::getList(['SORT' => 'ASC'], $arFilter, false, false, $arSelect);
        $arResult = [];
        while ($arItem = $res->Fetch()) {
            $arResult[$arItem['ID']] = $arItem;
        }

        if ($arResult && $isSection) {
            $res = SectionElementTable::getList([
                'select' => ['IBLOCK_ELEMENT_ID', 'IBLOCK_SECTION_ID'],
                'filter' => ['IBLOCK_ELEMENT_ID' => array_keys($arResult)],
            ]);
            $arData = [];
            while ($arItem = $res->fetch()) {
                $arData[$arItem['IBLOCK_SECTION_ID']][$arItem['IBLOCK_ELEMENT_ID']] = $arResult[$arItem['IBLOCK_ELEMENT_ID']];
                unset($arResult[$arItem['IBLOCK_ELEMENT_ID']]);
            }
            if($arResult) {
                $arData[0] = $arResult;
            }
            return $arData;
        }
        return $arResult;
    }

}