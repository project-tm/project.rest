<?php

namespace Project\Editor\Model\MasterBlock;

use CIBlockSection;
use Exception;
use Project\Editor\Helper;

class Rubric
{

    /**
     * текущий инфоблок
     *
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static protected function getIblockId()
    {
        return Helper\Iblock::getMasterBlockId();
    }

    /**
     * фильтрация для списка
     *
     * @param $arItem
     *
     * @return array
     */
    static protected function filterItem($arItem)
    {
        return [
            'ID'     => (int)$arItem['ID'],
            'PARENT' => (int)$arItem['IBLOCK_SECTION_ID'],
            'ACTIVE' => $arItem['ACTIVE'] === 'Y',
            'SORT'   => (int)$arItem['SORT'],
            'NAME'   => $arItem['NAME'],
        ];
    }

    /**
     * @param $ID
     *
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function getById($ID)
    {
        $arSelect = [
            'ID',
            'IBLOCK_SECTION_ID',
            'ACTIVE',
            'SORT',
            'NAME',
        ];
        $arFilter = ['IBLOCK_ID' => static::getIblockId(), 'ID' => $ID];
        $res = CIBlockSection::GetList(['SORT' => 'ASC'], $arFilter, false, $arSelect);
        if ($arItem = $res->Fetch()) {
            return static::filterItem($arItem);
        }
        return false;
    }

    /**
     * @param $ID
     * @param $arFields
     *
     * @throws Exception
     */
    static public function update($ID, $arFields)
    {
        if ($arFields['PARENT']) {
            $arFilter = [
                'IBLOCK_ID' => static::getIblockId(),
                'ID'        => $arFields['PARENT'],
            ];
            if (Helper\Iblock::isNetSection($arFilter)) {
                throw new Exception('Раздел не найден', 400);
            }
        }
        $arFilter = [
            'IBLOCK_ID'  => static::getIblockId(),
            '!ID'        => $ID,
            '=NAME'      => $arFields['NAME'],
            'SECTION_ID' => $arFields['PARENT'],
        ];
        if (Helper\Iblock::isSection($arFilter)) {
            throw new Exception('Раздел уже существует', 400);
        }
        $bs = new CIBlockSection;
        $arFields = [
            'IBLOCK_SECTION_ID' => (int)$arFields['PARENT'],
            'ACTIVE'            => $arFields['ACTIVE'] ? 'Y' : 'N',
            'SORT'              => (int)$arFields['SORT'] ?: 500,
            'NAME'              => $arFields['NAME'],
        ];
        $res = $bs->Update($ID, $arFields);
        if (empty($res)) {
            throw new Exception($bs->LAST_ERROR, 400);
        }
    }

    /**
     * @param $arFields
     *
     * @return bool|int
     * @throws Exception
     */
    static public function create($arFields)
    {
        if ($arFields['PARENT']) {
            $arFilter = [
                'IBLOCK_ID' => static::getIblockId(),
                'ID'        => $arFields['PARENT'],
            ];
            if (Helper\Iblock::isNetSection($arFilter)) {
                throw new Exception('Раздел не найден', 400);
            }
        }
        $arFilter = [
            'IBLOCK_ID'  => static::getIblockId(),
            '=NAME'      => $arFields['NAME'],
            'SECTION_ID' => (int)$arFields['PARENT'],
        ];
        if (Helper\Iblock::isSection($arFilter)) {
            throw new Exception('Раздел уже существует', 400);
        }

        $bs = new CIBlockSection;
        $arFields = [
            'IBLOCK_ID'         => static::getIblockId(),
            'IBLOCK_SECTION_ID' => (int)$arFields['PARENT'],
            'ACTIVE'            => $arFields['ACTIVE'] ? 'Y' : 'N',
            'SORT'              => (int)$arFields['SORT'] ?: 500,
            'NAME'              => $arFields['NAME'],
        ];
        $ID = $bs->add($arFields);
        if (empty($ID)) {
            throw new Exception($bs->LAST_ERROR, 400);
        }
        return $ID;
    }

    /**
     * @param $ID
     *
     * @throws Exception
     */
    static public function delete($ID)
    {
        $rsParentSection = CIBlockSection::GetByID($ID);
        if ($arParentSection = $rsParentSection->Fetch()) {
            $arFilter = [
                'IBLOCK_ID'     => $arParentSection['IBLOCK_ID'],
                '>LEFT_MARGIN'  => $arParentSection['LEFT_MARGIN'],
                '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],
                '>DEPTH_LEVEL'  => $arParentSection['DEPTH_LEVEL'],
            ];
            $rsSect = CIBlockSection::GetList(['left_margin' => 'asc'], $arFilter, false, ['ID'], ['nPageSize' => 1]);
            if ($arSect = $rsSect->Fetch()) {
                throw new Exception('У раздела есть подразделы', 400);
            }
            CIBlockSection::Delete($ID);
        } else {
            throw new Exception('Раздел не найден', 404);
        }
    }

}